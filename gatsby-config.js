/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Services CHATONS`,
    description: `Un sélecteur aléatoire de services libres pour décentraliser et découvrir un web à l'échelle humaine, solidaire et respectueux de votre vie privée !`,
    author: `@chatons`,
  },
  pathPrefix: `/entraide-chatons`,
  plugins: [
    {
      resolve: `gatsby-plugin-react-intl`,
      options: {
        // language JSON resource path
        path: `${__dirname}/src/intl`,
        // supported language
        languages: [`en`, `eo`, `fr`, `oc`, `de`, `it`, `hr`],
        // language file path
        defaultLanguage: `fr`,
        // option to redirect to `/ko` when connecting `/`
        redirect: true,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/markdown-pages`,
        name: `markdown-pages`,
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Services libres en ligne",
        description: `Des services libres en ligne du collectif CHATONS pour décentraliser et découvrir un web à l'échelle humaine, solidaire et respectueux de votre vie privée.`,
        short_name: "chatons",
        start_url: "/",
        background_color: "#fff",
        theme_color: "#a46708",
        lang: `fr`,
        display: `standalone`,
        icon: `static/favicon.png`,
        localize: [
          {
            start_url: `/en/`,
            lang: `en`,
            name: `Free-software online services`,
            short_name: `chatons`,
            description: `Free-software services from the CHATONS collective, to decentralize and discover a web on a human scale, united and respectful of your privacy.`,
          },
          {
            start_url: `/oc/`,
            lang: `oc`,
            name: `Servicis liures en linha`,
            short_name: `chatons`,
            description: `Un selector aleatòri de servicis liures per descentralizar e descobrir un web a l’escala umana, solidari e respectuòs de vòstra vida privada.`,
          },
          {
            start_url: `/de/`,
            lang: `de`,
            name: `Freie Software Onlinedienste`,
            short_name: `chatons`,
            description: `Freie Softwaredienste vom CHATONS Kollektiv, um das Web zu dezentralisieren und auf einem menschlichen Maß, vereint und respektvoll ihrer Privatsphäre zu entdecken.`,
          },
        ],
      },
    },
    "gatsby-plugin-catch-links",
    "gatsby-plugin-offline",
    {
      resolve: "gatsby-plugin-matomo",
      options: {
        siteId: "75",
        matomoUrl: "https://stats.framasoft.org",
        siteUrl: "https://entraide.chatons.org",
        matomoPhpScript: "p.php",
        matomoJsScript: "p.js",
      },
    },
  ],
}
