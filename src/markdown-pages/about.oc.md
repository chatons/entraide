---
title: "À propos"
date: "2020-03-31T12:30:03.284Z"
---

_Suite au confinement dû à l'épidémie de COVID-19, vous êtes nombreu⋅ses à avoir besoin d'outils pour continuer à communiquer avec vos proches et/ou télétravailler. Le Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires - CHATONS - met en place ce portail d’accès simplifié à des services en ligne sans inscription et répondant aux besoins les plus courants._

L'ensemble de ces services sont proposés par des hébergeurs CHATONS qui s'engagent à respecter la [charte du collectif](https://www.chatons.org/charte). Initié en 2016 par Framasoft, ce collectif rassemble à ce jour plus de 70 structures qui proposent des services en ligne libres, éthiques, décentralisés et solidaires afin de permettre aux internautes de trouver rapidement des alternatives aux services proposés pas les géants du web.

Nous prévoyons de maintenir cette page au-delà du temps de confinement et même de l'enrichir dans les semaines / mois qui viennent afin de vous proposer à terme d'autres outils libres proposés par les chatons, y compris ceux nécessitant inscription.
