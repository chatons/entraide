---
title: "Pri"
date: "2020-03-31T12:30:03.284Z"
---

_Post la karantenado pro la epidemio de COVID-19, multaj el vi bezonas ilojn por daŭrigi komunikon kun viaj karaĵoj aŭ labori hejme. La Kolektivo de Sendependaj, Travidebla, Malfermaj, Neŭtralaj, kaj Unuiĝintaj Gastigantoj - CHATONS - starigas tiun pordon por simpligi aliron al retejoj, kiuj plenumas la plej komunajn bezonojn sen postulo de registriĝo._

Ĉiuj ĉi tiuj servoj estas ofertitaj de gastigantoj de CHATONS, kiuj promesas respekti [la ĉarto de la kolektivo](https://www.chatons.org/eo/charte). Komencita en 2016 de Framasoft, tiu kolektivo nun kunigas pli ol 70 strukturojn, kiuj ofertas liberecan, etikan, decentralizitan kaj solidarecan retejo-servon por ebligi uzantojn de la Interreto rapide trovi alternativojn al la servoj proponitaj de la retegaj gigantoj.

Ni planas daŭrigi ĉi tiun paĝon post la karanten-tempo kaj eĉ aldoni pli da enhavojn en la venontaj semajnoj/montoj por fine doni al vi aliron al aliaj liberaj iloj gastigitaj de CHATONS, inkluzive de tiuj, kiuj postulas registriĝon.
