---
title: "About"
date: "2020-03-31T12:30:03.284Z"
---

_Nach der Quarantäne aufgrund des COVID-19-Ausbruchs brauchen viele von euch Hilfsmittel, um weiterhin mit euren Angehörigen zu kommunizieren und/oder von zu Hause aus zu arbeiten. Das Kollektiv unabhängiger, transparenter, offener, neutraler und vereinigter Hoster - CHATONS - hat dieses Portal eingerichtet, um den Zugang zu Online-Diensten zu vereinfachen, die häufigsten Bedürfnisse erfüllen, ohne dass eine Registrierung erforderlich ist._

Alle diese Dienste werden von CHATONS-Hostern angeboten, die sich verpflichten, [die Charta des Kollektivs](https://www.chatons.org/en/charte) zu respektieren. Das 2016 von Framasoft initiierte Kollektiv vereint derzeit mehr als 70 Strukturen, die kostenlose, ethische, dezentrale und solidarische Online-Dienste anbieten, damit Internetnutzer/innen schnell Alternativen zu den Diensten der Web-Giganten finden können.

Wir planen, diese Seite über die Quarantänezeit hinaus beizubehalten und in den kommenden Wochen/Monaten weitere Inhalte hinzuzufügen, um dir schließlich Zugang zu anderen kostenlosen Tools zu geben, die von den CHATONS gehostet werden, einschließlich derer, die eine Registrierung erfordern.
