---
title: "Informacije"
date: "2020-03-31T12:30:03.284Z"
---

_Nakon karantene zbog izbijanja COVID-19, mnogi od vas trebaju alate za nastavljanje komuniciranja sa svojim voljenima i/ili za rad od kuće. Kolektiv neovisnih, transparentnih, otvorenih, neutralnih i ujedinjenih hosting usluga – CHATONS – postavlja ovaj portal kako bi pojednostavio pristup internetskim uslugama koje zadovoljavaju najčešće potrebe bez registracije._

Sve ove usluge nude CHATONS usluge hostinga koji se obvezuju na poštivanje [statuta kolektiva](https://www.chatons.org/en/charte). Pokrenut 2016. od Framasofta, ovaj kolektiv trenutačno okuplja više od 70 struktura koje nude besplatne, etičke, decentralizirane i solidarne internetske usluge kako bi korisnicima interneta omogućile brzo pronalaženje alternativnih usluga koje nude velika poduzeća web-usluga.

Planiramo održavati ovu stranicu nakon karantene te dodati više sadržaja u sljedećim tjednima/mjesecima kako bismo omogućili pristup drugim besplatnim alatima koje hostira CHATONS, uključujući one koji zahtijevaju registraciju.
