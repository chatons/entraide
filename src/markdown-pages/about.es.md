---
title: "About"
date: "2020-03-31T12:30:03.284Z"
---

_Después de la cuarentena por el brote de COVID-19, muchos de ustedes necesitan herramientas para seguir comunicándose con sus seres queridos y / o trabajando desde casa. El Colectivo de Hosters Independientes, Transparentes, Abiertos, Neutrales y Unidos - CHATONS - está configurando este portal para simplificar el acceso a los servicios en línea que satisfacen las necesidades más comunes sin necesidad de registrarse._

Todos estos servicios son ofrecidos por hosters de CHATONS que se comprometen a respetar [los estatutos del colectivo](https://www.chatons.org/en/charte). Iniciado en 2016 por Framasoft, este colectivo agrupa actualmente a más de 70 estructuras que ofrecen servicios online gratuitos, éticos, descentralizados y solidarios para que los internautas encuentren rápidamente alternativas a los servicios que ofrecen los gigantes de la web.

Planeamos mantener esta página más allá del tiempo de cuarentena e incluso agregar más contenido en las próximas semanas/meses para eventualmente darle acceso a otras herramientas gratuitas alojadas por CHATONS, incluidas aquellas que requieren registro.
